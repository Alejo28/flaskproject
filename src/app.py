from flask import Flask, jsonify, request
from flask_restful import Resource, Api, reqparse
from sqlalchemy import create_engine 
from json import dumps
from UsuarioDAO import Usuario
app = Flask(__name__)
user = Usuario()

@app.route('/home', methods=['GET'])
def home():
    return jsonify({'mensaje': 'Bienvenido a flask'})   


@app.route('/usuario/listar', methods=['GET']) 
def users():
    try:
        rows = user.readAll()
        respuesta = jsonify(rows)
        respuesta.status_code = 200
        return respuesta
    except Exception as e:
        print(e)      

@app.route('/usuario/buscar/<int:id>')
def buscarusu(id):
    try:
        user.idusuario = id
        row = user.buscarusuario()
        resp = jsonify(row)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)           

@app.route('/usuario/create', methods=['POST'])
def agregarusuario():
    try:
        _json = request.json
        user.nomuser = _json['p_nomuser']
        user.clave = _json['p_clave']
        if request.method == 'POST':
            resp = user.agregarusuario()
            resp = jsonify('usuario')
            resp.status_code = 200
        return resp
    except Exception as e:
        print(e)        

@app.route('/usuario/eliminar/<int:id>', methods=['GET'])
def eliminarusuario(id):
    try:
        user.idusuario = id
        resp = user.delete()
        resp = jsonify('Usuario eliminado')
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)   

@app.route('/usuario/modificar', methods=['PUT'])    
def modificarusuario():
    try:
        _json = request.json
        user.nomuser = _json['p_nomuser']
        user.clave = _json['p_clave']
        user.idusuario = _json['p_idusuario']
        if request.method == 'PUT':
            resp = user.modificarusuario()
            resp = jsonify('Usuario Modificado')
            resp.status_code = 200
            return resp
    except Exception as e:
        print(e)        


if __name__ == '__main__':
    app.run(host='localhost', port=4000, debug=True)
    user = Usuario()    