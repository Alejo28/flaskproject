from flask import Flask, jsonify, request
from flask_restful import Resource, Api, reqparse
from sqlalchemy import create_engine
from json import dumps
from ProductoDao import Producto
app = Flask(__name__)
product = Producto()

@app.route('/home', methods=['GET'])
def home():
    return jsonify({'mensaje': 'esta es la home'})


@app.route('/producto/listar', methods=['GET'])
def products():
    try:
        rows = product.readAll()
        respuesta = jsonify(rows)
        respuesta.status_code = 200
        return respuesta
    except Exception as e:
        print(e)    

@app.route('/producto/buscar/<int:id>')
def buscarpro(id):
    try:
        product.idproducto = id
        row = product.buscarproducto()
        resp = jsonify(row)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e) 

@app.route('/producto/create', methods=['POST'])
def agregarproducto():
    try:
        _json = request.json
        product.nomproducto = _json['p_nomproducto']
        product.precio = _json['p_precio'] 
        product.cantidad = _json['p_cantidad']
        if request.method == 'POST':
            resp = product.agregarproducto()
            resp = jsonify('Producto creado...')
            resp.status_code = 200
        return resp
    except Exception as e:
        print(e)

@app.route('/producto/eliminar/<int:id>', methods=['GET'])
def eliminarproducto(id):
    try:
        product.idproducto = id
        resp = product.delete()
        resp = jsonify('Producto Eliminado')
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)            

@app.route('/producto/modificar', methods=['PUT'])
def modificarproducto():
    try:
        _json = request.json
        product.nomproducto = _json['p_nomproducto']
        product.precio = _json['p_precio']
        product.cantidad = _json['p_cantidad']
        product.idproducto = _json['p_idproducto']
        if request.method == 'PUT':
            resp = product.modificarprodcuto()
            resp = jsonify('Producto modificado')
            resp.status_code = 200
            return resp 
    except Exception as e:
        print(e)        



             


if __name__ == '__main__':
    app.run(host='localhost', port=4000, debug=True)
    product = Producto()